import { Slot } from "./signal";
export declare class Observable<T> {
    #private;
    /**
     * @param value Initial value
     */
    constructor(value: T);
    set value(value: T);
    get value(): T;
    /**
     * Subscribes the given function to the observable and returns an unsubscribing function.
     * Calls the given function with the current value of the observable.
     * @param slot function called whenever the observable changes
     * @returns  unsubscribing function
     */
    subscribe(slot: Slot<T>): () => void;
    /**
     * Subscribes the given function to the observable and returns an unsubscribing function.
     * @param  slot function called whenever the observable changes
     * @returns unsubscribing function
     */
    onChanged(slot: Slot<T>): () => void;
}
//# sourceMappingURL=observable.d.ts.map