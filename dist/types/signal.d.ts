export type Slot<T> = (value: T) => void;
export declare class Signal<T> {
    #private;
    /**
     * Subscribes the given function to the signal and returns an unsubscribing function
     * @param slot
     * @returns function called whenever the signal is raised.
     */
    subscribe(slot: Slot<T>): () => void;
    /**
     * Raises a new value. All listeners will be called with this value as parameter.
     * @param value Value passed to the listeners.
     */
    emit(value: T): void;
    /**
     * Alias for `subscribe`. Subscribes the given function to the signal and returns an unsubscribing function
     * @param  slot function called whenever the signal is raised.
     * @returns  unsubscribing function
     */
    on(slot: Slot<T>): () => void;
}
//# sourceMappingURL=signal.d.ts.map