import { Signal, Slot } from "./signal";
import { Observable } from "./observable";
export type Feeder<T> = (update: (value: T) => void, error: (message?: string) => void) => (() => void);
export declare class Observer<T> extends Observable<T> {
    #private;
    error: Signal<string | undefined>;
    /**
     * @param value Initial value
     */
    constructor(value: T, feed: Feeder<T>, options?: {
        lazy: boolean;
    });
    /**
     * Subscribes the given function to the observable and returns an unsubscribing function.
     * Calls the given function with the current value of the observable.
     * @param slot function called whenever the observable changes
     * @returns  unsubscribing function
     */
    subscribe(slot: Slot<T>): () => void;
    /**
     * Subscribes the given function to the observable and returns an unsubscribing function.
     * @param  slot function called whenever the observable changes
     * @returns unsubscribing function
     */
    onChanged(slot: Slot<T>): () => void;
}
//# sourceMappingURL=observer.d.ts.map