// src/signal.ts
var Signal = class {
  #slots = [];
  /**
   * Removes the given function from the listeners
   * @param slot the function to be removed
   */
  #unsubscribe(slot) {
    this.#slots = this.#slots.filter((item) => item !== slot);
  }
  /**
   * Subscribes the given function to the signal and returns an unsubscribing function
   * @param slot 
   * @returns function called whenever the signal is raised.
   */
  subscribe(slot) {
    this.#slots = [...this.#slots, slot];
    return () => this.#unsubscribe(slot);
  }
  /**
   * Raises a new value. All listeners will be called with this value as parameter.
   * @param value Value passed to the listeners.
   */
  emit(value) {
    this.#slots.forEach((slot) => slot(value));
  }
  /**
   * Alias for `subscribe`. Subscribes the given function to the signal and returns an unsubscribing function
   * @param  slot function called whenever the signal is raised.
   * @returns  unsubscribing function
   */
  on(slot) {
    return this.subscribe(slot);
  }
};

// src/observable.ts
var Observable = class {
  #value;
  #changed = new Signal();
  /**
   * @param value Initial value
   */
  constructor(value) {
    this.#value = value;
  }
  set value(value) {
    if (value === this.#value)
      return;
    this.#value = value;
    this.#changed.emit(this.#value);
  }
  get value() {
    return this.#value;
  }
  /**
   * Subscribes the given function to the observable and returns an unsubscribing function.
   * Calls the given function with the current value of the observable.
   * @param slot function called whenever the observable changes
   * @returns  unsubscribing function
   */
  subscribe(slot) {
    slot(this.#value);
    return this.#changed.on(slot);
  }
  /**
   * Subscribes the given function to the observable and returns an unsubscribing function.
   * @param  slot function called whenever the observable changes
   * @returns unsubscribing function
   */
  onChanged(slot) {
    return this.#changed.on(slot);
  }
};

// src/observer.ts
var Observer = class extends Observable {
  #feed;
  #unFeed;
  #slotCount = 0;
  #lazy;
  error = new Signal();
  /**
   * @param value Initial value
   */
  constructor(value, feed, options) {
    super(value);
    this.#feed = feed;
    this.#lazy = options?.lazy ?? false;
    if (!this.#lazy)
      this.#unFeed = this.#feed(
        (value2) => this.value = value2,
        (message) => this.error.emit(message)
      );
  }
  /**
   * Subscribes the given function to the observable and returns an unsubscribing function.
   * Calls the given function with the current value of the observable.
   * @param slot function called whenever the observable changes
   * @returns  unsubscribing function
   */
  subscribe(slot) {
    if (this.#slotCount == 0 && this.#lazy) {
      this.#unFeed = this.#feed(
        (value) => this.value = value,
        (message) => this.error.emit(message)
      );
    }
    const unsubscribe = super.subscribe(slot);
    this.#slotCount++;
    return () => {
      unsubscribe();
      this.#slotCount--;
      if (this.#slotCount == 0 && this.#lazy) {
        this.#unFeed();
      }
    };
  }
  /**
   * Subscribes the given function to the observable and returns an unsubscribing function.
   * @param  slot function called whenever the observable changes
   * @returns unsubscribing function
   */
  onChanged(slot) {
    if (this.#slotCount == 0 && this.#lazy) {
      this.#unFeed = this.#feed(
        (value) => this.value = value,
        (message) => this.error.emit(message)
      );
    }
    const unsubscribe = super.onChanged(slot);
    this.#slotCount++;
    return () => {
      unsubscribe();
      this.#slotCount--;
      if (this.#slotCount == 0 && this.#lazy) {
        this.#unFeed();
      }
    };
  }
};
export {
  Observable,
  Observer,
  Signal
};
//# sourceMappingURL=index.js.map
