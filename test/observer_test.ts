import { Test } from "@ersbeth/picotest";
import assert from "node:assert/strict";
import { Observable, Observer, Signal } from "@ersbeth/picosignal";

const test = Test.fromUrl(import.meta.url);

test.add("slot is called each time value is modified", () => {

    let computed = 0;
    const observable = new Observable<number>(1);

    const observer = new Observer<number>(
        observable.value,
        (update) => {
            return observable.onChanged(update)
        }
    )

    observer.onChanged((value) => { computed = value * 2; });

    assert.equal(computed, 0);
    observable.value = 2;
    assert.equal(computed, 4);
    observable.value = 5;
    assert.equal(computed, 10);
});

test.add("not lazy always updates", () => {

    const observable = new Observable<number>(1);

    const observer = new Observer<number>(
        observable.value,
        (update) => {
            return observable.onChanged((value) => {
                update(2 * value)
            })
        }
    )

    assert.equal(observer.value, 1);
    observable.value = 2;
    assert.equal(observer.value, 4);

    let computed = 0;
    observer.onChanged((value) => { computed = value * 2; });

    observable.value = 5;
    assert.equal(observer.value, 10);
    assert.equal(computed, 20);
});

test.add("lazy updates only when subscribed", () => {

    const observable = new Observable<number>(1);

    const observer = new Observer<number>(
        observable.value,
        (update) => {
            return observable.onChanged((value) => {
                update(2 * value)
            })
        },
        { lazy: true }
    )


    // doesnt update  
    assert.equal(observer.value, 1);
    observable.value = 2;
    assert.equal(observer.value, 1);

    let computed = 0;
    const unsubscribe = observer.onChanged((value) => { computed = value * 2; });

    // update
    observable.value = 5;
    assert.equal(observer.value, 10);
    assert.equal(computed, 20);

    // doesnt update
    unsubscribe();
    observable.value = 1;
    assert.equal(observer.value, 10);
    assert.equal(computed, 20);

});

test.add("subscribe calls the slot with current value", () => {

    let computed = 0;
    let observable = new Observable(1);

    const observer = new Observer<number>(
        2 * observable.value,
        (update) => {
            return observable.onChanged((value) => {
                update(2 * value)
            })
        },
    )

    observer.subscribe((value) => { computed = value * 2; });

    assert.equal(computed, 4);
    observable.value = 2;
    assert.equal(computed, 8);
    observable.value = 5;
    assert.equal(computed, 20);
});

test.add("errors are raised", () => {

    const observable = new Observable(1);
    const errorSignal = new Signal<string>();

    const observer = new Observer<number>(
        2 * observable.value,
        (update, error) => {
            errorSignal.on((message) => error(message));
            return observable.onChanged((value) => {
                update(2 * value)
            })
        },
    )
    let error = "";
    observer.error.on(message => error = message!);

    assert.equal(error, "");

    observable.value = 2;
    assert.equal(observer.value, 4);

    errorSignal.emit("error1")
    assert.equal(error, "error1");
});

test.run();