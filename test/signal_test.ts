import { Test } from "@ersbeth/picotest";
import assert from "node:assert/strict";
import { Signal } from "@ersbeth/picosignal";

const test = Test.fromUrl(import.meta.url);

test.add("slot is called each time signal is raised", () => {

    let data = 0;
    let signal = new Signal<void>();
    signal.on(() => { data++ });

    assert.equal(data, 0);
    signal.emit();
    assert.equal(data, 1);
    signal.emit();
    assert.equal(data, 2);
});

test.add("signal parameter is passed to slot", () => {

    let data = 0;
    let signal = new Signal<number>();
    signal.on((value) => { data = value });

    assert.equal(data, 0);
    signal.emit(3);
    assert.equal(data, 3);
});

test.add("multiple slots", () => {

    let data = 0;
    let signal = new Signal<number>();
    signal.on((value) => { data = value });
    signal.on(() => { data *= 2 });

    assert.equal(data, 0);
    signal.emit(3);
    assert.equal(data, 6);
});

test.add("unsubscribe", () => {
    let data = 0;
    let signal = new Signal<void>();
    let unsubscribe = signal.on(() => { data++ });

    assert.equal(data, 0);
    signal.emit();
    assert.equal(data, 1);
    unsubscribe();
    signal.emit();
    assert.equal(data, 1);
})

test.run();