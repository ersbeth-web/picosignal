# PicoSignal

Minimal signal library written in typescript. 

[Signal programming](https://en.wikipedia.org/wiki/Signal_programming) is a particular type of [event driven programming](https://en.wikipedia.org/wiki/Event-driven_programming). This library provides: 

* signals and slots (aka. events emitting and callbacks)
* observable values with auto-emitted signals on set

This library runs both in browser and on Node.js

## Installation
```  npm install @ersbeth/picosignal```

## Usage

### Signal

#### Basic signal

Declare a new signal and register a slot (aka. callback) to it. Then the slot will be called every time the signal is emitted :
```ts
let data = 0;
const signal = new Signal<void>();
signal.on(() => { data++ });

assert.equal(data, 0);
signal.emit();
assert.equal(data, 1);
signal.emit();
assert.equal(data, 2);
```

#### Signal with parameter

You can provide a parameter when emitting a signal. The parameter will be forwarded to the slot:

```ts
let data = 0;
const signal = new Signal<number>();
signal.on((value:number) => { data = value });

assert.equal(data, 0);
signal.emit(3);
assert.equal(data, 3);
```

#### Signal with multiple slots
You can provide multiple slots to a signal. They will be called by order of registration:

```ts
let data1 = 0;
let data2 = 2;
const signal = new Signal<number>();
signal.on((value:number) => { data1 += value });
signal.on((value:number) => { data2 *= value });

assert.equal(data1, 0);
assert.equal(data2, 2);
signal.emit(3);
assert.equal(data1, 3);
assert.equal(data2, 6);
```

#### Unsubscribe

The registering function returns an unregistering function which can be called at anytime to remove the connection:

```ts
    let data = 0;
    let signal = new Signal<void>();
    let unsubscribe = signal.on(() => { data++ });

    assert.equal(data, 0);
    signal.emit();
    assert.equal(data, 1);
    unsubscribe();
    signal.emit();
    assert.equal(data, 1); // data was not increased
```

### Observable
#### Registration without execution

An `Observable` contains a `value` and a signal named `onChanged` that is automatically emitted each time `value` is set :

```ts
let computed = 0;
const observable = new Observable<number>(1);
observable.onChanged((value:number) => { computed = value * 2; });

assert.equal(computed, 0); // not 2, slot hasn't been called
observable.value = 2;
assert.equal(computed, 4);
observable.value = 5;
assert.equal(computed, 10);
```

Notice that the slot is NOT called with the current value upon registration. See `subscribe` below if you need such behaviour.

#### Registration with execution
 
You can use the `subscribe` method if you want both to:
* register your slot to an observable
* execute your slot with the current value of the observable

```ts
let computed = 0;
let observable = new Observable<number>(1);
observable.subscribe((value:number) => { computed = value * 2; });

assert.equal(computed, 2); // here slot has been called
observable.value = 2;
assert.equal(computed, 4);
observable.value = 5;
assert.equal(computed, 10);
```

