import { Signal, Slot } from "./signal";
import { Observable } from "./observable";

export type Feeder<T> = (
    update: (value: T) => void,
    error: (message?: string) => void
) => (() => void)

export class Observer<T> extends Observable<T>{

    #feed: Feeder<T>;
    #unFeed: (() => void) | undefined;
    #slotCount = 0;
    #lazy: boolean;

    error: Signal<string | undefined> = new Signal();

    /**
     * @param value Initial value
     */
    constructor(value: T, feed: Feeder<T>, options?: { lazy: boolean }) {
        super(value);
        this.#feed = feed;
        this.#lazy = options?.lazy ?? false;
        if (!this.#lazy) this.#unFeed = this.#feed(
            (value: T) => this.value = value,
            (message?: string) => this.error.emit(message))
    }

    /**
     * Subscribes the given function to the observable and returns an unsubscribing function.
     * Calls the given function with the current value of the observable.
     * @param slot function called whenever the observable changes
     * @returns  unsubscribing function
     */
    subscribe(slot: Slot<T>): () => void {
        if (this.#slotCount == 0 && this.#lazy) {
            this.#unFeed = this.#feed(
                (value: T) => this.value = value,
                (message?: string) => this.error.emit(message))
        }

        const unsubscribe = super.subscribe(slot);
        this.#slotCount++;

        return () => {
            unsubscribe();
            this.#slotCount--;
            if (this.#slotCount == 0 && this.#lazy) {
                this.#unFeed!();
            }
        }
    }

    /**
     * Subscribes the given function to the observable and returns an unsubscribing function.
     * @param  slot function called whenever the observable changes
     * @returns unsubscribing function
     */
    onChanged(slot: Slot<T>): () => void {
        if (this.#slotCount == 0 && this.#lazy) {
            this.#unFeed = this.#feed(
                (value: T) => this.value = value,
                (message?: string) => this.error.emit(message))
        }
        const unsubscribe = super.onChanged(slot);
        this.#slotCount++;

        return () => {
            unsubscribe();
            this.#slotCount--;
            if (this.#slotCount == 0 && this.#lazy) {
                this.#unFeed!();
            }
        }
    }
}