export type Slot<T> = (value: T) => void

export class Signal<T> {
    #slots: Slot<T>[] = [];

    /**
     * Removes the given function from the listeners
     * @param slot the function to be removed
     */
    #unsubscribe(slot: Slot<T>): void {
        this.#slots = this.#slots.filter((item) => item !== slot);
    }

    /**
     * Subscribes the given function to the signal and returns an unsubscribing function
     * @param slot 
     * @returns function called whenever the signal is raised.
     */
    subscribe(slot: Slot<T>): () => void {
        this.#slots = [...this.#slots, slot];
        return () => this.#unsubscribe(slot);
    }

    /**
     * Raises a new value. All listeners will be called with this value as parameter.
     * @param value Value passed to the listeners.
     */
    emit(value: T): void {
        this.#slots.forEach((slot) => slot(value));
    }

    /**
     * Alias for `subscribe`. Subscribes the given function to the signal and returns an unsubscribing function
     * @param  slot function called whenever the signal is raised.
     * @returns  unsubscribing function
     */
    on(slot: Slot<T>): () => void {
        return this.subscribe(slot);
    }
}
