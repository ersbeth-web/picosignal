import { Signal, Slot } from "./signal";

export class Observable<T> {
    #value;
    #changed = new Signal<T>();

    /**
     * @param value Initial value
     */
    constructor(value: T) {
        this.#value = value;
    }

    set value(value: T) {
        if (value === this.#value) return;
        this.#value = value;
        this.#changed.emit(this.#value);
    }

    get value(): T {
        return this.#value;
    }

    /**
     * Subscribes the given function to the observable and returns an unsubscribing function.
     * Calls the given function with the current value of the observable.
     * @param slot function called whenever the observable changes
     * @returns  unsubscribing function
     */
    subscribe(slot: Slot<T>): () => void {
        slot(this.#value);
        return this.#changed.on(slot);
    }

    /**
     * Subscribes the given function to the observable and returns an unsubscribing function.
     * @param  slot function called whenever the observable changes
     * @returns unsubscribing function
     */
    onChanged(slot: Slot<T>): () => void {
        return this.#changed.on(slot);
    }
}
